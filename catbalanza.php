<?php 
session_start();
if(!isset($_SESSION['userid'])){
    header('Location: ./login');
}else{
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    $shortname = $_SESSION['shortname'];
	$shortlastname = $_SESSION['shortlastname'];
    $permisos = $_SESSION['tipous'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './include/head.php'?>
    <!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <?php include './include/loader.php'?>
    <div id="main-wrapper">
        <?php include './include/header.php'?>
        <?php include './include/nav.php'?>
        <?php include './include/catalogos/catbalanza.php'?>
    </div>
    <?php include './include/scripts.php'?>
    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
    <script>
    $(document).ready(function() {
        var fileInput = $('.custom-file-input');

        fileInput.on('change', function() {
            var filesCount = $(this)[0].files.length;
            var textContainer = $(".custom-file-label");


            if (filesCount === 1) {
                // if single file then show file name
                textContainer.html($(this).val().split('\\').pop());
            }else {
                // otherwise show number of files
                textContainer.text(filesCount + ' files selected');
            }
        });
    });

    $("#show").click(function() {
        $("#show-table").click()
    });

    $("#upload").click(function() {
        if($("#file").val() == ""){
            Swal.fire({
                    icon: 'warning',
                    title: 'Oops!',
                    text: 'Debes seleccionar un archivo',
                    confirmButtonText: 'Ok'
                });
        }else{
            $("#submit").click()
        }
    });
</script>
</body>
</html>