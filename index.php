<?php 
session_start();
if(!isset($_SESSION['userid'])){
    header('Location: ./login');
}else{
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    $shortname = $_SESSION['shortname'];
	$shortlastname = $_SESSION['shortlastname'];
    $permisos = $_SESSION['tipous'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './include/head.php'?>
</head>
<body>
    <?php include './include/loader.php'?>
    <div id="main-wrapper">
        <?php include './include/header.php'?>
        <?php include './include/nav.php'?>
        <?php include './include/dashboard/index.php'?>
    </div>
    <?php include './include/scripts.php'?>
</body>
</html>