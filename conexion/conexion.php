<?php
$config = include('config.php');

if(!($conectar = mysqli_connect($config->HOST,$config->USER,$config->PASS))){
	printf("Ocurrió un error en la conexión.", mysqli_connect_errno());
	exit();
	}
if(!(mysqli_select_db($conectar,$config->BASE))){
	printf("Error seleccionando la base de datos.");
	exit();
	}
return $conectar;
?>
