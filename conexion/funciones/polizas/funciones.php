<?php
//Obtiene clave de sucursal desde sucursal-(en excel)
function obtener_clave_sucursal($data){
    $suc = str_replace("DISTRIBUIDOR ","",$data);
    $clave = find_one_clave_sucursal($suc);

    return $clave;
}
//Obtiene segmento de sucursal desde sucursal-(en excel)
function obtener_segmento_sucursal($data){
    if($data == "CONCENTRADORA"){
        $clave = " ";
    }else{
        $suc = str_replace("DISTRIBUIDOR ","",$data);
        $clave = find_one_segmento_sucursal($suc);
    }
    return $clave;
}
//Obtiene Clave de distribuidor desde folio
function obtener_tipo_factura($folio, $clave){
    $cadena = preg_replace('/[0-9]+/', '', $folio);
    $limpia = str_replace("-","",$cadena);
    if($limpia == "NOID"){
        $return = false;
    }else{
        $return = str_replace($clave,"",$limpia);
    }
    //$clave = substr($limpia, -2);
    return $return;
}





//Obtiene Clave de segmento desde folio
function obtener_clave_segmento($folio){
    var_dump($folio);
    $cadena = preg_replace('/[0-9]+/', '', $folio);
    $limpia = str_replace("-","",$cadena);
    $clave = substr($limpia, -1);
    return $clave;
}
//Crea tabla de poliza para su descarga
function table($data){
    $table = '<div class="table-responsive">
                        <table id="poliza" class="table header-border table-responsive-sm">
                              <thead>
                                  <tr>
                                    <th>Cuenta</th>
                                    <th>Nombre</th>
                                    <th>Cargo</th>
                                    <th>Abono</th>
                                    <th>Cargo MN</th>
                                    <th>Abono MN</th>
                                    <th>Referencia</th>
                                    <th>Concepto</th>
                                    <th>Diario</th>
                                    <th>Seg. Neg.</th>
                                  </tr>
                              </thead>
                              <tbody>';

        foreach ($data as $key => $val) {
            $cuentaBanco = $data[$key]['cuentabanco'];
            $nombreCuentaBanco = $data[$key]['nombre'];
            $cargo = $data[$key]['cargo'];
            $referencia = $data[$key]['referencia'];
            $concepto = $data[$key]['concepto'];
            $diario = $data[$key]['diario'];
            $nosegmento = $data[$key]['nosegmento'];
            $rubro = $data[$key]['rubro'];
            $tipoFac = $data[$key]['tipofac'];
            $cuentaCargo = $data[$key]['cuentacargo'];
            $cuentaAbono = $data[$key]['cuentaabono'];
            $cuentaIvaCargo = $data[$key]['cuentaivacargo'];
            $cuentaIvaAbono = $data[$key]['cuentaivaabono'];
            $base = $data[$key]['base'];
            $iva = $data[$key]['iva'];

            $table .= '<tr>';
            $table .= '<td>'.$cuentaBanco.'</td>';
            $table .= '<td>'.$nombreCuentaBanco.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($cargo, 2).'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$referencia.'</td>';
            $table .= '<td>'.$referencia.' '.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            foreach ($data[$key]['abonos'] as $i => $val) {
                $table .= '<tr>';
                $table .= '<td>'.$val['cuentacliente'].'</td>';
                $table .= '<td></td>';
                $table .= '<td></td>';
                if($val['abonous'] == 0 || $val['abonous'] == ""){
                    $table .= '<td></td>';
                }else{
                    $table .= '<td>'.number_format($val['abonous'], 2).'</td>';
                }
                $table .= '<td></td>';
                if($tipoFac == "FN"){
                    $table .= '<td>'.number_format($val['abonop'], 2).'</td>';
                }else{
                    $table .= '<td>'.number_format($val['abonot'], 2).'</td>';
                }
                $table .= '<td>'.$referencia.'</td>';
                if($val['folioFactura'] == false){
                    $folioFactura = $referencia;
                    $table .= '<td>'.$referencia.' '.$concepto.'</td>';
                }else{
                    $folioFactura = $val['folioFactura'];
                    $table .= '<td>'.$val['folioFactura'].' '.$concepto.'</td>';
                }
                $table .= '<td></td>';
                $table .= '<td>'.$nosegmento.'</td>';
                $table .= '</tr>';

                $ivafac = $val['iva'];
            }

            if($tipoFac != "AN"){
            $table .= '<tr>';
            $table .= '<td>'.$cuentaCargo.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($base, 2).'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$referencia.'</td>';
            $table .= '<td>'.$folioFactura.' '.$concepto.'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaAbono.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($base, 2).'</td>';
            $table .= '<td>'.$referencia.'</td>';
            $table .= '<td>'.$folioFactura.' '.$concepto.'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            if($ivafac > 0){
                    if($tipoFac != "FN"){
                        $table .= '<tr>';
                        $table .= '<td>'.$cuentaIvaCargo.'</td>';
                        $table .= '<td></td>';
                        $table .= '<td></td>';
                        $table .= '<td></td>';
                        $table .= '<td>'.number_format($iva, 2).'</td>';
                        $table .= '<td></td>';
                        $table .= '<td>'.$referencia.'</td>';
                        $table .= '<td>'.$folioFactura.' '.$concepto.'</td>';
                        $table .= '<td></td>';
                        $table .= '<td>'.$nosegmento.'</td>';
                        $table .= '</tr>';
                    }
        
                    $table .= '<tr>';
                    $table .= '<td>'.$cuentaIvaAbono.'</td>';
                    $table .= '<td></td>';
                    $table .= '<td></td>';
                    $table .= '<td></td>';
                    $table .= '<td></td>';
                    $table .= '<td>'.number_format($iva, 2).'</td>';
                    $table .= '<td>'.$referencia.'</td>';
                    $table .= '<td>'.$folioFactura.' '.$concepto.'</td>';
                    $table .= '<td></td>';
                    $table .= '<td>'.$nosegmento.'</td>';
                    $table .= '</tr>';
                }
            }

            


    }
    $table .= "</tbody>";
    $table .= "</table></div></div></div>";
    return $table;
}
//Obtiene cuenta de cuenta bancaria
function cuenta_banco($data){
    $cban = (string) filter_var($data, FILTER_SANITIZE_NUMBER_INT);
    $cbannum = str_replace("-","",$cban);
    $result = find_one_cuenta($cbannum);
    if($result != false){
        $return = str_replace("-","",$result);
    }else{
        $return = "";
    }
    
    return $return;
}
//Obtiene descripcion de cuenta bancaria
function nombre_cuenta_banco($data){
    $cban = (string) filter_var($data, FILTER_SANITIZE_NUMBER_INT);
    $cbannum = str_replace("-","",$cban);
    $result = find_one_cuenta($cbannum);
    if($result != false){
        $return = find_nombre_cuenta($result);
    }else{
        $return = "";
    }

    return $return;
}
//Obtiene segmento de factura desde concepto
function obtener_segmento($concepto){
    $pos = stristr($concepto, "TZ");
    $cadena = preg_replace('/[0-9]+/', '', $pos);
    $limpia = str_replace("-","",$cadena);
    $segmento = str_replace("TZ","",$limpia);
    return $segmento;
}
//Obtener rubro de factura (A = Maquinaria, B = Refacciones, C = Servicio)
function obtener_rubro($concepto, $clave){
    $pos = stristr($concepto, $clave);
    $cadena = preg_replace('/[0-9]+/', '', $pos);
    $limpia = str_replace("-","",$cadena);
    $return = str_replace($clave,"",$limpia);
    return $return;
}
//Obtener rubro de factura desde rubro-Excel (A = Maquinaria, B = Refacciones, C = Servicio)
function obtener_rubro_excel($rubro){
        switch (trim($rubro)) {
            case "MAQ":
                $return = "A";
            break;
            case "REF":
                $return = "B";
            break;
            case "SER":
                $return = "C";
            break;
            default:
              $return = "";
          }
    return $return;
}
//Obtiene folio de factura pagada desde el concepto
function obtener_folio_factura($concepto, $clave, $rubro){
    //$rubro = obtener_rubro($concepto,$clave);
    if($rubro != ""){
        $pos = stristr($concepto, $clave.$rubro);
        if($pos === FALSE) {
            $return = false;
          }else{
            $return = $pos;
          }
    }else{
        $return = false;
    }
    
    //$cadena = preg_replace('/[0-9]+/', '', $pos);
    //$limpia = str_replace("-","",$cadena);
    //$return = str_replace($clave.$rubro,"",$limpia);

    return $return;
}
//
/*function cuenta_ingresos_cobrados($pumn,$ivamn){
    if($ivamn == 0){
        $ingresos = 0;
      }else{
        $ingresos = round(($ivamn * 100)/$pumn);
      }
    
      $cuenta = '';
    
      switch (intval($ingresos)) {
        case 0:
            $cuenta = "7-1-01-01-02-0000";
        break;
        case 8:
          $cuenta = "7-1-01-01-03-0000";
        break;
        case 16:
            $cuenta = "7-1-01-01-01-0000"; 
        break;
    }
    return $cuenta;
}*/
//Obtiene cuenta de cliente desde BD
function cuenta_cliente($cliente, $rubro, $iva, $concepto, $tipofac){

    if($tipofac == 'RP'){
        switch ($rubro) {
            case "A":
                if($iva > 0){
                  $return = "110501010001";
                }else{
                  $return = '110501030001';
                }
            break;
            case "B":
              $return = "110501070001";
            break;
            case "C":
              $return = "110501090001"; 
            break;
            default:
              $return = false;
        }
    }else if($tipofac == 'FN'){
            $ant = strpos($concepto, "ANTICIPO");
            if($ant === false){
                $cuenta = find_cuenta_cliente_segmento($cliente,$rubro,$iva);
                if($cuenta == false){
                    $return = 'No se encontro una cuenta en la BD';
                }else{
                    $return = str_replace("-","",$cuenta);
                }
            }else{
                switch ($rubro) {
                    case "A":
                        if($iva > 0){
                          $return = "210301030001";
                        }else{
                          $return = '210301010001';
                        }
                    break;
                    case "B":
                      $return = "210301040002";
                    break;
                    case "C":
                      $return = "210301050005"; 
                    break;
                    default:
                      $return = false;
                }
          }
        }else if($tipofac == 'AN'){
            $return = "210201010039";
        }else{
            $cuenta = find_cuenta_cliente_segmento($cliente,$rubro,$iva);
            if($cuenta == false){
                $return = 'No se encontro una cuenta en la BD';
            }else{
                $return = str_replace("-","",$cuenta);
            }
      }


    /*$ant = strpos($concepto, "ANTICIPO");
    if($ant === false) {
        $apl = strpos($concepto, "APLICA");
        if($apl == false){
            $conpto = false;
        }else{
            $conpto = "APLICA";
        }
    }else{
        $conpto = "ANTICIPO";
    }
    $cuenta = find_cuenta_cliente_segmento($cliente,$rubro,$iva, $conpto);
    if($cuenta == false){
        $return = "";
    }else{
        $return = str_replace("-","",$cuenta);
    }*/


    return $return;
}
//Obtiene cuenta del cargo base IVA
function cuenta_cargo($pumn,$ivamn){
    if($ivamn == 0){
        $ingresos = 0;
      }else{
        $ingresos = round(($ivamn * 100)/$pumn);
      }
      $return="";
      switch (intval($ingresos)) {
        case 0:
            $return = "710101020000";
        break;
        case 8:
          $return = "710101030000";
        break;
        case 16:
            $return = "710101010000"; 
        break;
    }

    return $return;
}
//Obtiene cuenta del abono base IVA
function cuenta_abono($pumn,$ivamn){
    if($ivamn == 0){
        $ingresos = 0;
      }else{
        $ingresos = round(($ivamn * 100)/$pumn);
      }
    
      $return = '';
    
      switch (intval($ingresos)) {
        case 0:
            $return = "810101020000";
        break;
        case 8:
          $return = "810101030000";
        break;
        case 16:
            $return = "810101010000"; 
        break;
    }

    //$return = str_replace("-","",$cuenta);
    return $return;
}
//Obtiene cuenta del IVA en el campo cargo
function cuenta_iva_cargo($pumn,$ivamn){
    if($ivamn == 0){
        $ingresos = 0;
      }else{
        $ingresos = round(($ivamn * 100)/$pumn);
      }
    
      $cuenta = '';
    
      switch (intval($ingresos)) {
        case 8:
          $cuenta = "2-1-07-01-04-0001";
        break;
        case 16:
            $cuenta = "2-1-07-01-03-0001"; 
        break;
    }

    $return = str_replace("-","",$cuenta);
    return $return;
}
//Obtiene cuenta del IVA en el campo abono
function cuenta_iva_abono($pumn,$ivamn, $folio){
    if($ivamn == 0){
        $ingresos = 0;
      }else{
        $ingresos = round(($ivamn * 100)/$pumn);
        //var_dump($folio."<br/>");
        //var_dump($ingresos."<br/>");
        //var_dump($pumn."<br/>");
        //var_dump($ivamn."<br/>");
      }
    
      $cuenta = '';
    
      switch (intval($ingresos)) {
        case 8:
            $cuenta = "2-1-08-01-04-0001";
        break;
        case 16:
              $cuenta = "2-1-08-01-03-0001"; 
        break;
    }

    $return = str_replace("-","",$cuenta);
    return $return;
}
function obtener_diario($rubro, $iva){
    if($rubro == ""){
        $return = $rubro;
      }else{
        switch ($rubro) {
            case "MAQ":
                if($iva > 0){
                    $return = "2";
                  }else{
                    $return = "1";
                  }
            break;
            case "A":
                if($iva > 0){
                    $return = "2";
                  }else{
                    $return = "1";
                  }
            break;
            case "REF":
                $return = "4";
            break;
            case "B":
                $return = "4";
            break;
            case "SER":
                $return = "5";
            break;
            case "C":
                $return = "5";
            break;
            default:
              $return = "3";
          }
      }
    return $return;
}

//Obtiene segmento de sucursal desde sucursal-(en excel) para poliza DOF Servicios
function obtener_segmento_sucursal_dof_servicio($data){
    
    switch($data){
        case 'ACA':
            $sucursal = 'ACAYUCAN';
        break;
        case 'CAMP':
            $sucursal = 'CAMPECHE';
        break;
        case 'CAN':
            $sucursal = 'CANCUN';
        break;
        case 'CHET':
            $sucursal = 'CHETUMAL';
        break;
        case 'ISL':
            $sucursal = 'ISLA';
        break;
        case 'MER':
            $sucursal = 'MERIDA';
        break;
        case 'SIAP':
            $sucursal = 'SIAP';
        break;
        case 'TAP':
            $sucursal = 'TAPACHULA';
        break;
        case 'TBCA':
            $sucursal = 'TIERRA BLANCA';
        break;
        case 'TIZ':
            $sucursal = 'TIZIMIN';
        break;
        case 'TXG':
            $sucursal = 'ACAYUTUXTLACAN';
        break;
        case 'VER':
            $sucursal = 'VERACRUZ';
        break;
        case 'VILL':
            $sucursal = 'VILLAHERMOSA';
        break;
        case 'ZAP':
            $sucursal = 'ACAYUCAN';
        break;
        default:
            $sucursal = $data;
    }
        $clave = find_one_segmento_sucursal_clave($sucursal);

    return $clave;
}

//Crea tabla de poliza de ventas de servicio para su descarga
function table_poliza_ventas_servicio($data){
    $table = '<div class="table-responsive">
                        <table id="polizaventser" class="table header-border table-responsive-sm">
                              <thead>
                                  <tr>
                                    <th>Cuenta</th>
                                    <th>Nombre</th>
                                    <th>Cargo ME</th>
                                    <th>Abono ME</th>
                                    <th>Cargo</th>
                                    <th>Abono</th>
                                    <th>Referencia</th>
                                    <th>Concepto</th>
                                    <th>Diario</th>
                                    <th>Seg. Neg.</th>
                                  </tr>
                              </thead>
                              <tbody>';

        foreach ($data as $key => $val) {

            if($val['impuestos'] == 0){
                $ingresos = 0;
              }else{
                $ingresos = round(($val['impuestos'] * 100)/$val['subtotal']);
              }
            
            
              switch (intval($ingresos)) {
                case 0:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IVA TRASLADADO POR COBRAR 0%';
                break;
                case 8:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IVA TRASLADADO POR COBRAR 8%'; 
                break;
                case 16:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IIVA TRASLADADO POR COBRAR 16%'; 
                break;
            }

            switch ($val['division']) {
                case "AGRICOLA":
                    $cuenta = "4108010900";
                break;
                case "CONSTRUCCION":
                      $cuenta = "4108010700"; 
                break;
                case "COMPACTACION":
                    $cuenta = "4108010700"; 
              break;
                case "DRONES":
                    $cuenta = "4108010300";
                break;
                case "SIAP":
                    $cuenta = "4108010300";
                break;
                case "INDUSTRIAL":
                      $cuenta = "4108010700"; 
                break;
                case "JARDINERIA":
                    $cuenta = "4108011000"; 
              break;
              case "JARDINERIA & GOLF":
                  $cuenta = "4108011000";
              break;
              case "GOLF":
                $cuenta = "4108011000";
            break;
              case "TRITURACION":
                    $cuenta = "4108010700"; 
              break;
              default:
                    $cuenta = '0';
            }

            switch ($val['cond']) {
                case 'CO':
                    if($val['moneda'] == 'USD'){
                        $cuentaCliente = '110501100006';
                    }else{
                        $cuentaCliente = '110501090001';
                    }
                    $nombreCliente = 'CLIENTE DE CONTADO';
                break;
                case 'LCR':
                    $ccliente = find_cuenta_cliente_segmento($val['cliente'],"C",intval($ingresos));
                    if($ccliente){
                        $cuentaCliente = str_replace("-","",$ccliente);
                    }else{
                        $cuentaCliente = "Sin datos en BD";
                    }
                    //$cuentaCliente = '110501090000';
                    $nombreCliente = 'LINEA DE CREDITO';
                break;
                
                default:
                    $cuentaCliente = '0';
                    $nombreCliente = 'S/D';
                break;
            }

            //$cuentaIVATras = '210701030001';
            //$nombreIVAtras = 'IVA TRASLADADO POR COBRAR 0%';
            $cuentaIVARet = '112101040001';
            $nombreIVARet = 'IVA RETENCION 6%';
            $cuentaMano = $cuenta.'01';
            $nombreMano = 'MANO DE OBRA';
            $cuentaKilo = $cuenta.'02';
            $nombreKilo = 'KILOMETRAJE';
            $cuentaFora = $cuenta.'04';
            $nombreFora = 'TRABAJOS FORANEOS';
            $cuentaOtros = $cuenta.'05';
            $nombreOtros = 'OTROS';
            $cuentaRefac = '410701010015';
            $nombreRefac = 'VENTA DE REFACCIONES POR SERVICIO 16%';
            $cuentaDesc = '412101010009';
            $nombreDesc = 'DESCUENTO';
            $folioNumero = preg_replace('/[^0-9]/', '', $val['folio']);
            $folioTexto = preg_replace('/[0-9]+/', '', $val['folio']);
            $folio = $folioTexto.ltrim($folioNumero, '0');
            $concepto = $folio.' '.$val['cliente'];
            $diario = '';
            $nosegmento = find_one_segmento_sucursal_clave(substr($folioTexto, 0, -2));

            $table .= '<tr>';
            $table .= '<td>'.$cuentaCliente.'</td>';
            $table .= '<td>'.$nombreCliente.'</td>';
            if($val['moneda'] == 'USD'){
                $table .= '<td>'.number_format($val['totalfactura']/$val['tdc']).'</td>';
            }else{
                $table .= '<td></td>';
            }
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['totalfactura'], 2).'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaIVATras.'</td>';
            $table .= '<td>'.$nombreIVAtras.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['impuestos'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaIVARet.'</td>';
            $table .= '<td>'.$nombreIVARet.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format(0, 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaMano.'</td>';
            $table .= '<td>'.$nombreMano.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['manoobra'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaKilo.'</td>';
            $table .= '<td>'.$nombreKilo.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['kilometraje'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaFora.'</td>';
            $table .= '<td>'.$nombreFora.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['tforaneo'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaOtros.'</td>';
            $table .= '<td>'.$nombreOtros.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['varios'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaRefac.'</td>';
            $table .= '<td>'.$nombreRefac.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['refacciones'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaDesc.'</td>';
            $table .= '<td>'.$nombreDesc.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['descuento'], 2).'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';
        }
    $table .= "</tbody>";
    $table .= "</table></div></div></div>";
    return $table;
}


//Retorna fecha  en formato desde excel
function transform_fecha($date){

    $fechaText = preg_replace('/[0-9]+/', '', $date);

    switch (trim($fechaText)) {
        case "Ene":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '01', $dateTranform);
        break;
        case "Feb":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '02', $dateTranform); 
        break;
        case "Mar":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '03', $dateTranform);
      break;
        case "Abr":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '04', $dateTranform);
        break;
        case "May":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '05', $dateTranform);
        break;
        case "Jun":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '06', $dateTranform);
        break;
        case "Jul":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '07', $dateTranform);
      break;
      case "Ago":
        $dateTranform = str_replace(' ', '-', $date);
        $return = $fecha_trans = str_replace(trim($fechaText), '08', $dateTranform);
      break;
      case "Sep":
        $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '09', $dateTranform);
    break;
      case "Oct":
        $dateTranform = str_replace(' ', '-', $date);
        $return = $fecha_trans = str_replace(trim($fechaText), '10', $dateTranform); 
      break;
      case "Nov":
        $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '11', $dateTranform); 
        break;
        case "Dic":
            $dateTranform = str_replace(' ', '-', $date);
            $return = $fecha_trans = str_replace(trim($fechaText), '12', $dateTranform); 
      break;
      default:
            $return = '01-01-2022';
    }

    return $return;
}
//Crea tabla de poliza de ventas de refacciones para su descarga
function table_poliza_ventas_refacciones($data){
    $table = '<div class="table-responsive">
                        <table id="polizaventser" class="table header-border table-responsive-sm">
                              <thead>
                                  <tr>
                                    <th>Cuenta</th>
                                    <th>Nombre</th>
                                    <th>Cargo ME</th>
                                    <th>Abono ME</th>
                                    <th>Cargo</th>
                                    <th>Abono</th>
                                    <th>Referencia</th>
                                    <th>Concepto</th>
                                    <th>Diario</th>
                                    <th>Seg. Neg.</th>
                                  </tr>
                              </thead>
                              <tbody>';

        foreach ($data as $key => $val) {

            if($val['iva'] == 0){
                $ingresos = 0;
              }else{
                $ingresos = round(($val['iva'] * 100)/$val['subtotal']);
              }
            
            
              switch (intval($ingresos)) {
                case 0:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IVA TRASLADADO POR COBRAR 0%';
                break;
                case 8:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IVA TRASLADADO POR COBRAR 8%'; 
                break;
                case 16:
                    $cuentaIVATras = '210701030001';
                    $nombreIVAtras = 'IVA TRASLADADO POR COBRAR 16%'; 
                break;
            }


            switch ($val['codventa']) {
                case 'CO':
                    if($val['moneda'] == 'USD'){
                        $cuentaCliente = '110501100006';
                    }else{
                        $cuentaCliente = '110501070001';
                    }
                    $nombreCliente = 'CLIENTE DE CONTADO';
                break;
                case 'CR':
                    $ccliente = find_cuenta_cliente_segmento($val['cliente'],"B",intval($ingresos));
                    if($ccliente){
                        $cuentaCliente = str_replace("-","",$ccliente);
                    }else{
                        $cuentaCliente = "Sin datos en BD";
                    }
                    //$cuentaCliente = '110501090000';
                    $nombreCliente = $val['cliente'];
                break;
                
                default:
                    $cuentaCliente = '0';
                    $nombreCliente = 'S/D';
                break;
            }

            $folioNumero = preg_replace('/[^0-9]/', '', $val['folio']);
            $folioTexto = preg_replace('/[0-9]+/', '', $val['folio']);
            $folio = $folioTexto.ltrim($folioNumero, '0');
            $concepto = $folio.' '.$val['cliente'];
            $diario = '';
            $nosegmento = find_one_segmento_sucursal_clave(substr($folioTexto, 0, -2));

            $table .= '<tr>';
            $table .= '<td>'.$cuentaCliente.'</td>';
            $table .= '<td>'.$nombreCliente.'</td>';
            if($val['moneda'] == 'USD'){
                $table .= '<td>'.number_format($val['subtotal']/$val['dof']).'</td>';
            }else{
                $table .= '<td></td>';
            }
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['total'], 2).'</td>';
            $table .= '<td></td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>'.$cuentaIVATras.'</td>';
            $table .= '<td>'.$nombreIVAtras.'</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['iva'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            $table .= '<tr>';
            $table .= '<td>410701010010</td>';
            $table .= '<td>VENTA DE REFACCIONES 16%</td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td></td>';
            $table .= '<td>'.number_format($val['sub'], 2).'</td>';
            $table .= '<td>'.$folio.'</td>';
            $table .= '<td>'.$concepto.'</td>';
            $table .= '<td>'.$diario.'</td>';
            $table .= '<td>'.$nosegmento.'</td>';
            $table .= '</tr>';

            if($val['descuento']){
                $table .= '<tr>';
                $table .= '<td>412101010001</td>';
                $table .= '<td>REFACCIONES 16%</td>';
                $table .= '<td></td>';
                $table .= '<td></td>';
                $table .= '<td>'.number_format($val['descuento'], 2).'</td>';
                $table .= '<td></td>';
                $table .= '<td>'.$folio.'</td>';
                $table .= '<td>'.$concepto.'</td>';
                $table .= '<td>'.$diario.'</td>';
                $table .= '<td>'.$nosegmento.'</td>';
                $table .= '</tr>';
            }

        }
    $table .= "</tbody>";
    $table .= "</table></div></div></div>";
    return $table;
}
?>