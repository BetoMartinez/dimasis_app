<?php
    include '../insert.php';
    include '../update.php';
    include '../login/en-uncrypt.php';

    $respuesta='';
    $modo = $_POST['modo'];

	if ($modo == 1) {
        $id = $_POST['id'];
		$datos = [
			"nombre" => utf8_decode($_POST['nombre']),
			"apellido" =>utf8_decode($_POST['apellido']),
			"correo" => utf8_decode($_POST['correo']),
			"tipous" => intval($_POST['tipous']),
		];
		$respuesta = update_usuario($datos,$id);
	}else{
		$passEncrypt = encrypt($_POST['contrasena']);
		$datos = [
			    "nombre" => utf8_decode($_POST['nombre']),
				"apellido" =>utf8_decode($_POST['apellido']),
				"correo" => utf8_decode($_POST['correo']),
				"contrasena" => $passEncrypt,
				"tipous" => intval($_POST['tipous']),
		];
        $respuesta = insert_nuevo_usuario($datos);
	}
				
	echo $respuesta;

?>
