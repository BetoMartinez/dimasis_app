<?php
function insert_nueva_cuenta_balanza($cuenta, $descripcion){
    include './conexion/conexion.php';

    $sql = "INSERT INTO catalogos_balanza (cuenta,descripcion,estatus) 
		    VALUES ('".$cuenta."','".$descripcion."',1)";
	
    $result = $conectar->query($sql);
	$ok = ($result) ? true : false;
	$conectar->close();
    $return = ($ok) ? 'Itrue' : 'Ifalse';

    return $return;
}
function insert_nuevo_usuario($params){
    include '../../conexion_control.php';

    $nombre = $params['nombre'];
    $apellido = $params['apellido'];
    $correo = $params['correo']."@dimasur.com.mx";
    $contrasena = $params['contrasena'];
    $tipous = $params['tipous'];

    $sql = "INSERT INTO usuarios (nombre, apellido, correo, contrasena, id_tipoUsuario, id_empresa, fechaAlta, estatus) 
		    VALUES ('".$nombre."','".$apellido."', '".$correo."', '".$contrasena."', ".$tipous.", 1, CURRENT_DATE, 1)";
	
    $result = $conectar->query($sql);
    if (! $result) {
        $return = "Error en la inserción: " . $conectar->error;
    }else{
        $ok = ($result) ? true : false;
        $return = ($ok) ? 1 : 0;
    }
    $conectar->close();

    return $return;
}
function insert_nueva_sucursal($params){
    include '../../conexion.php';

    $clave = $params['clave'];
    $sucursal = $params['suc'];
    $segmento = $params['seg'];

    $sql = "INSERT INTO cat_sucursales (sucursal, clave, segmento, estatus) 
		    VALUES ('".$sucursal."','".$clave."', '".$segmento."', 1)";
	
    $result = $conectar->query($sql);
    if (! $result) {
        $return = "Error en la inserción: " . $conectar->error;
    }else{
        $ok = ($result) ? true : false;
        $return = ($ok) ? 1 : 0;
    }
    $conectar->close();

    return $return;
}
function insert_nuevo_excel_facs($mesEmision,$anioEmision,$granTotal,$ruta_excel,$ruta_json,$fileName){
    include './conexion/conexion.php';

    $sql = "INSERT INTO cat_fac_emitidas (emisor, rfc_emisor, lugar_expedicion, mes, anio, total, ruta_excel, ruta_json, nombre_archivo, fecha_alta, estatus) 
		    VALUES ('DISTRIBUIDOR DE MAQUINARIA DEL SUR', 'DMS000928DF8', 1, ".$mesEmision.", ".$anioEmision." ,".$granTotal.", '".$ruta_excel."', '".$ruta_json."', '".$fileName."', CURRENT_DATE, 1)";
	 
    $result = $conectar->query($sql);
    if (! $result) {
        $return = "Error en la inserción: " . $conectar->error;
    }else{
        $ok = ($result) ? true : false;
        $return = ($ok) ? 1 : 0;
    }
    $conectar->close();

    return $return;
}
?>