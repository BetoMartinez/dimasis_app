<?php
include './conexion/funciones/polizas/funciones.php';



/*START POLIZA COBROS*/
function find_one_clave_sucursal($data){
  include './conexion/conexion.php';

  $sql = "SELECT id
          ,sucursal 
          ,clave 
      FROM cat_sucursales
      WHERE sucursal LIKE '%".trim($data)."%'";

  $result = $conectar->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $return = $row["clave"];
    }
  }else{
    $return = false;
  }
  $conectar->close();
  return $return;
}
function find_one_segmento_sucursal($data){
  include './conexion/conexion.php';

  $sql = "SELECT id
          ,sucursal 
          ,segmento 
      FROM cat_sucursales
      WHERE sucursal LIKE '%".trim($data)."%'";

  $result = $conectar->query($sql);

  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()) {
      $return = $row["segmento"];
    }
  }else{
    $return = false;
  }
  $conectar->close();
  return $return;
}
function find_one_segmento_sucursal_clave($data){
  include './conexion/conexion.php';

  $sql = "SELECT id
          ,clave 
          ,segmento 
      FROM cat_sucursales
      WHERE clave LIKE '%".trim($data)."%'";

  $result = $conectar->query($sql);

  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()) {
      $return = $row["segmento"];
    }
  }else{
    $return = false;
  }
  $conectar->close();
  return $return;
}
function find_one_cuenta($data){
  include './conexion/conexion.php';

  $sql = "SELECT 
            id, 
            cuenta, 
            descripcion
        FROM catalogos_balanza
        WHERE descripcion LIKE '%".$data."%'";


  $result = $conectar->query($sql);

  if($result->num_rows > 0){
      while($row = $result->fetch_assoc()) {
        $return = $row["cuenta"];
      }
  }else{
    $return = false;
  }
  $conectar->close();
  return $return;
}
function find_cuenta_cliente_segmento($cliente,$rubro,$ivamn){
  include './conexion/conexion.php';

  switch ($rubro) {
    case "A":
        if($ivamn > 0){
          $seg = "1-1-05-01-01";
        }else{
          $seg = '1-1-05-01-03';
        }
    break;
    case "B":
      $seg = "1-1-05-01-07";
    break;
    case "C":
      $seg = "1-1-05-01-09"; 
    break;
    default:
      $seg = "";
  }

  if(trim($cliente) == "VENTA DE CONTADO" || trim($cliente) =="CLIENTE CONTADO" || trim($cliente) == "VENTA CONTADO"){
      $cuenta = $seg."0001";
  }else{
    if($rubro != ""){
      $sql = "SELECT 
              id, 
              cuenta, 
              descripcion
            FROM catalogos_balanza 
            WHERE cuenta LIKE '%".$seg."%' AND descripcion LIKE '%".trim($cliente)."%'";
  
      $result = $conectar->query($sql);
  
      if ($result->num_rows > 0) {
        $cuenta= false;
    
        while($row = $result->fetch_assoc()) {
          $cuenta = $row["cuenta"];
        }
      }else{
        $cuenta = false;
      }
      $conectar->close();
    }else{
      $cuenta = false;
    }
  }
  return $cuenta;
}
function find_nombre_ingresos_cobrados($pumn,$ivamn){
  include './conexion/conexion.php';

  $cuenta = cuenta_ingresos_cobrados($pumn,$ivamn);
  $sql = "SELECT 
            id, 
            cuenta, 
            descripcion
          FROM catalogos_balanza 
          WHERE cuenta = '".$cuenta."'";

  $result = $conectar->query($sql);

  if($result->num_rows > 0) {
    $nombre='';
    while($row = $result->fetch_assoc()) {
      $nombre = $row["descripcion"];
    }
  }else{
    $nombre = false;
  }
  $conectar->close();
  return $nombre;
}
function find_nombre_cuenta($cuenta){
  include './conexion/conexion.php';

  $sql = "SELECT 
            id, 
            cuenta, 
            descripcion
          FROM catalogos_balanza 
          WHERE cuenta = '".$cuenta."'";


  $result = $conectar->query($sql);

  if($result->num_rows > 0) {
    $nombre='';
    while($row = $result->fetch_assoc()) {
      $nombre = $row["descripcion"];
    }
  }else{
    $nombre = false;
  }
  $conectar->close();
  return $nombre;
}
/*END POLIZA COBROS*/

/*START CATALOGO BALANZA*/
function select_cat_sucursales(){
	include './conexion/conexion.php';

  $sql = "SELECT 
            id, 
            sucursal, 
            clave,
            segmento,
            estatus
        FROM cat_sucursales";
  $result = $conectar->query($sql);
  $tabla='';

  if ($result->num_rows > 0) {
    $i=1;
    $tabla='';

    while($row = $result->fetch_assoc()) {
      $estatus = ($row["estatus"] == 1) ? '<span class="badge light badge-success"><i class="fa fa-circle text-success mr-1"></i>Activo</span>' : '<span class="badge light badge-danger"><i class="fa fa-circle text-danger mr-1"></i>Inactivo</span>';  
        $tabla.= '<tr>
                      <td align="center">'.$i.'</td>
                      <td>'.$row["sucursal"].'</td>
                      <td align="center">'.$row["clave"].'</td>
                      <td align="center">'.$row["segmento"].'</td>
                      <td>'.$estatus.'</td>
                      <td class="text-center">
                        <div class="dropdown ml-auto text-center">
                          <div class="btn-link" data-toggle="dropdown">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                          </div>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Editar</a>
                          </div>
                        </div>
                      </td>
                      </tr>';
        $i++;
    }
  } else {
    echo false;
  }
  $conectar->close();
  return $tabla;
}
function select_cat_balanza(){
	include './conexion/conexion.php';

  $sql = "SELECT 
            id, 
            cuenta, 
            descripcion,
            estatus
        FROM catalogos_balanza";
  $result = $conectar->query($sql);
  $tabla='';

  if ($result->num_rows > 0) {
    $i=1;
    $tabla='';

    while($row = $result->fetch_assoc()) {
      $estatus = ($row["estatus"] == 1) ? '<span class="badge light badge-success"><i class="fa fa-circle text-success mr-1"></i>Activo</span>' : '<span class="badge light badge-danger"><i class="fa fa-circle text-danger mr-1"></i>Inactivo</span>';  
        $tabla.= '<tr>
                      <td align="center">'.$i.'</td>
                      <td>'.$row["descripcion"].'</td>
                      <td>'.$row["cuenta"].'</td>
                      <td>'.$estatus.'</td>
                      <td class="text-center">
                        <div class="dropdown ml-auto text-center">
                          <div class="btn-link" data-toggle="dropdown">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                          </div>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Editar</a>
                          </div>
                        </div>
                      </td>
                      </tr>';
        $i++;
    }
  } else {
    echo false;
  }
  $conectar->close();
  return $tabla;
}
function find_cuenta_cat_balanza($cuenta, $descripcion){
  include './conexion/conexion.php';
  include './conexion/funciones/update.php';
  include './conexion/funciones/insert.php';

  $sql = "SELECT 
            cuenta, 
            descripcion
        FROM catalogos_balanza
        WHERE cuenta LIKE '%".$cuenta."%'";


  $result = $conectar->query($sql);
  $return="";

  if($result->num_rows > 0){
      while($row = $result->fetch_assoc()) {
        if(trim($row["descripcion"]) != trim($descripcion)){
          $return = update_cuenta_balanza($cuenta, $descripcion);
        }else{
          $return = 'Omi';
        }
      }
  }else{
    $return = insert_nueva_cuenta_balanza($cuenta, $descripcion);
  }
  $conectar->close();
  return $return;
}
/*END CATALOGO BALANZA*/

/*START CATALOGO USUARIOS*/
function select_cat_usuarios(){
	include './conexion/conexion_control.php';

  $sql = "SELECT 
            id, 
            nombre, 
            apellido,
            correo,
            id_tipoUsuario,
            estatus
        FROM usuarios
        WHERE id_tipoUsuario <> 1";
  $result = $conectar->query($sql);
  $tabla='';

  if ($result->num_rows > 0) {
    $i=1;
    $tabla='';

    while($row = $result->fetch_assoc()) {
      $estatus = ($row["estatus"] == 1) ? '<span class="badge light badge-success"><i class="fa fa-circle text-success mr-1"></i>Activo</span>' : '<span class="badge light badge-danger"><i class="fa fa-circle text-danger mr-1"></i>Inactivo</span>';  
        $tabla.= '<tr>
                      <td align="center">'.$i.'</td>
                      <td>'.$row["nombre"].' '.$row["apellido"].'</td>
                      <td align="center">'.$row["correo"].'</td>
                      <td>'.$estatus.'</td>
                      <td class="text-center">
                        <div class="dropdown ml-auto text-center">
                          <div class="btn-link" data-toggle="dropdown">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                          </div>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" onclick=" return cargardatos('.intval($row["id"]).')" href="javascript:void()">Editar</a>
                            <a class="dropdown-item" onclick=" return cargarid('.intval($row["id"]).')" data-toggle="modal" data-target="#modalUsuarioContrasenia" href="javascript:void()">Cambiar Contraseña</a>
                          </div>
                        </div>
                      </td>
                      </tr>';
        $i++;
    }
  } else {
    echo false;
  }
  $conectar->close();
  return $tabla;
}
function get_usuario($id) {
  include '../../conexion_control.php';
  $sql = "SELECT id
                ,nombre
                ,apellido
                ,correo
                ,id_tipoUsuario
          FROM usuarios WHERE id =".$id;

  $result = $conectar->query($sql);
  if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
      
      $correo = str_replace("@dimasur.com.mx", "", $row["correo"]);
      $return= [
        "nombre" =>utf8_encode($row["nombre"]),
        "apellido" => utf8_encode($row["apellido"]),
        "correo" => $correo,
        "id_tipoUsuario" => intval($row["id_tipoUsuario"]),
      ];
    }
  }
  $conectar->close();
  return json_encode($return);
}

/*END CATALOGO USUARIOS*/

/*START CATALOGO_FAC_EMITIDAS*/
function select_cat_fac_emitidas(){
	include './conexion/conexion.php';

  $sql = "SELECT 
            nombre_archivo, 
            mes,
            anio,
            total,
            fecha_alta,
            estatus
        FROM cat_fac_emitidas
        WHERE estatus = 1";
  $result = $conectar->query($sql);
  $tabla='';

  if ($result->num_rows > 0) {
    $i=1;
    $tabla='';

    while($row = $result->fetch_assoc()) {
      $estatus = ($row["estatus"] == 1) ? '<span class="badge light badge-success"><i class="fa fa-circle text-success mr-1"></i>Activo</span>' : '<span class="badge light badge-danger"><i class="fa fa-circle text-danger mr-1"></i>Inactivo</span>';  
      $fechaAlta = new DateTime($row["fecha_alta"]);
      $fecha = $fechaAlta->format('d-m-Y');
      $mesDB = new DateTime('01-'.$row["mes"].'-'.$row["anio"]);
      $mes = $mesDB->format('M-Y');  
      $tabla.= '<tr>
                      <td align="center">'.$i.'</td>
                      <td>'.$row["nombre_archivo"].'</td>
                      <td align="center">'.$mes.'</td>
                      <td>$'.number_format($row["total"],2).'</td>
                      <td>'.$fecha.'</td>
                      <td>'.$estatus.'</td>
                      <td class="text-center">
                        <div class="dropdown ml-auto text-center">
                          <div class="btn-link" data-toggle="dropdown">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                          </div>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Editar</a>
                          </div>
                        </div>
                      </td>
                      </tr>';
        $i++;
    }
  } else {
    echo false;
  }
  $conectar->close();
  return $tabla;
}
function find_fac_emitidas_mes($mes, $anio){
  include './conexion/conexion.php';

  $sql = "SELECT 
            mes, 
            anio
        FROM cat_fac_emitidas
        WHERE mes = ".$mes." AND anio = ".$anio;


  $result = $conectar->query($sql);
  $return="";

  if($result->num_rows > 0){
      $return = false;
  }else{
    $return = true;
  }
  $conectar->close();
  return $return;
}
/*END CATALOGO_FAC_EMITIDAS*/
?>