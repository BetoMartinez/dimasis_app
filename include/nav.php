        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
				<ul class="metismenu" id="menu">
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-networking"></i>
							<span class="nav-text">Dashboard</span>
						</a>
                        <ul aria-expanded="false">
							<li><a href="./">Dashboard</a></li>
						</ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-notepad"></i>
							<span class="nav-text">Pólizas</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="./polizacobros">Póliza Cobros</a></li>
                            <?php //if($permisos <= 2){ ?>
                            <li><a href="./polizaventservicio">Poliza Ventas Servicio</a></li>
                            <li><a href="./polizaventrefac">Poliza Ventas Refacciones</a></li>
                            <?php //}?>
                        </ul>
                    </li>
                    <?php if($permisos <= 2){ ?>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-folder"></i>
							<span class="nav-text">Catálogos</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="./catusuarios">Catálogo Usuarios</a></li>
                            <li><a href="./catsucursales">Catálogo Sucursales</a></li>
                            <li><a href="./catbalanza">Catálogo Balanza</a></li>
                            <li><a href="./catfacemitidas">Catálogo Fac. Emitidas</a></li>
                            <!--<li><a href="./polizaventas">Poliza Ventas</a></li>-->
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            
				<div class="copyright">
					<p style="padding: 0 !important; margin: 0 !important"><strong>DIMASUR - APP</strong></p>
                    <p style="padding: 0 !important; margin: 0 !important">© 2022 All Rights Reserved</p>
					
				</div>
			</div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->