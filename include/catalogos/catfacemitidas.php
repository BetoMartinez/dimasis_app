<?php
set_time_limit(4000);
include './conexion/funciones/select.php';
//include './conexion/funciones/insert.php';
$show = false;
if(isset($_POST['show'])){

    $show = true;
}

if(isset($_POST['submit'])){
    
        $name = $_FILES['file']['name'];
        $tmp_name = $_FILES['file']['tmp_name'];
        $type = $_FILES['file']['type'];
                
        if($type == 'application/vnd.ms-excel'){
            // Extension excel 97
            $ext = 'xls';
        }else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            // Extension excel 2007 y 2010
            $ext = 'xlsx';
        }else{
            // Extension no valida
            $input = "Este archivo no es valido";
            echo "Este archivo no es valido";
            exit();
        }

	    $fileName = str_replace(".".$ext, "", $name);

	    require_once './vendor/PHPExcel/Classes/PHPExcel.php';
	    $archivo = $tmp_name;
	    $inputFileType = PHPExcel_IOFactory::identify($archivo);
	    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	    $objPHPExcel = $objReader->load($archivo);
	    $sheet = $objPHPExcel->getSheet(0); 
	    $highestRow = $sheet->getHighestRow(); 
	    $highestColumn = $sheet->getHighestColumn();

	    $i =1;
	    $utrue = 0;
	    $ufalse = 0;
        $itrue = 0;
        $ifalse = 0;
        $omi = 0;
        $data = array();
        $fechaEmision = '';
        $granTotal = 0;

	    for ($row = 2; $row <= $highestRow; $row++){
		    $estadoSAT = $sheet->getCell("B".$row)->getValue();
		    $version = $sheet->getCell("C".$row, )->getValue();
            $tipo = $sheet->getCell("D".$row)->getValue();
		    $fechaExcel = $sheet->getCell("E".$row, )->getValue();
            if($fechaExcel != ''){
                //$oridate = date_create($fechaExcel);
                //$newdate = date_format($oridate, 'Y/d/m');
                $fecha = str_replace('/', '-', $fechaExcel);
                $fechaEmision = new DateTime($fecha);
                $diaEmision = $fechaEmision->format('d');
                $mesEmision = $fechaEmision->format('m');
                $anioEmision = $fechaEmision->format('Y');
            }else{
                $fechaEmision = $fechaExcel;
            }
            
            
            $fechaTimbrado = $sheet->getCell("F".$row)->getValue();
		    $serie = $sheet->getCell("I".$row, )->getValue();
            $folio = $sheet->getCell("J".$row)->getValue();
		    $uuid = $sheet->getCell("K".$row, )->getValue();
            $uuidrelacion = $sheet->getCell("L".$row)->getValue();
		    $lugarExpe = $sheet->getCell("O".$row, )->getValue();
            $rfcReceptor = $sheet->getCell("P".$row)->getValue();
		    $nombreReceptor = $sheet->getCell("Q".$row, )->getValue();
            $usoCFDI = $sheet->getCell("T".$row)->getValue();
		    $subtotal = $sheet->getCell("U".$row, )->getValue();
            $descuento = $sheet->getCell("V".$row)->getValue();
		    $totalIEPS = $sheet->getCell("W".$row, )->getValue();
            $iva = $sheet->getCell("X".$row)->getValue();
            if($iva > 0){
                $ivaPorcentaje = round(($iva * 100)/($subtotal-$descuento));
            }else{
                $ivaPorcentaje = 0;
            }
            $ivaRetenido = $sheet->getCell("Y".$row)->getValue();
		    $isrRetenido = $sheet->getCell("Z".$row, )->getValue();
            $ish = $sheet->getCell("AA".$row)->getValue();
		    $total = $sheet->getCell("AB".$row, )->getValue();
            $totalOriginal = $sheet->getCell("AC".$row)->getValue();
		    $totalTrasladados = $sheet->getCell("AD".$row, )->getValue();
            $totalRetenidos = $sheet->getCell("AE".$row)->getValue();
		    $totalLocalTras = $sheet->getCell("AF".$row, )->getValue();
            $totalLocalRete = $sheet->getCell("AG".$row)->getValue();
		    $complemento = $sheet->getCell("AH".$row, )->getValue();
            $moneda = $sheet->getCell("AI".$row)->getValue();
		    $tipoCambio = $sheet->getCell("AJ".$row, )->getValue();
            $formaPago = $sheet->getCell("AK".$row, )->getValue();
            $metodoPago = $sheet->getCell("AL".$row)->getValue();
		    $condicionPago = $sheet->getCell("AN".$row, )->getValue();
            $conceptos = $sheet->getCell("AO".$row)->getValue();
            $conceptosArray = explode('*',$conceptos);

            array_push($data, 
							['estadosat' => $estadoSAT,
							'version' => $version,
							'tipo' => $tipo,
							'fechaemision' => $fechaEmision,
                            'diaemision' => $diaEmision,
                            'mesemision' => $mesEmision,
                            'anioemision' => $anioEmision,
							'fechatimbrado' => $fechaTimbrado,
							'serie' => $serie,
							'folio' => $folio,
							'uuid' => $uuid,
							'uuidrelacion' => $uuidrelacion,
							'lugarexpe' => $lugarExpe,
							'rfcreceptor' => $rfcReceptor,
							'nombrereceptor' => $nombreReceptor,
							'usocfdi' => $usoCFDI,
                            'subtotal' => $subtotal,
							'descuento' => $descuento,
							'totalieps' => $totalIEPS,
							'iva' => $iva,
							'ivaporcentaje' => $ivaPorcentaje,
							'ivaretenido' => $ivaRetenido,
							'isrretenido' => $isrRetenido,
							'ish' => $ish,
							'total' => $total,
							'totaloriginal' => $totalOriginal,
							'totaltrasladados' => $totalTrasladados,
							'totalretenidos' => $totalRetenidos,
                            'totallocaltras' => $totalLocalTras,
							'totallocalrete' => $totalLocalRete,
							'complemento' => $complemento,
							'moneda' => $moneda,
							'tipocambio' => $tipoCambio,
							'formapago' => $formaPago,
							'metodopago' => $metodoPago,
							'condicionpago' => $condicionPago,
							'conceptos' => $conceptosArray
							]);
            
            /*$revision = find_cuenta_cat_balanza($cuenta, $descripcion);

            switch ($revision) {
                case "Utrue":
                    $utrue++;
                break;
                case "Ufalse":
                  $ufalse++;
                break;
                case "Itrue":
                  $itrue++; 
                break;
                case "Ifalse":
                  $ifalse++;
                default:
                $omi++;
              }

		    $i++;*/
            $granTotal += $total;
	    }
        
        $revision = find_fac_emitidas_mes($mesEmision, $anioEmision);
        if($revision){
            $json = json_encode($data);
            $ruta_excel = 'files/facemitidas/'.$name;
            $ruta_json = 'files/facemitidas/'.$fileName.'.json';
            $sql = insert_nuevo_excel_facs($mesEmision,$anioEmision,$granTotal,$ruta_excel,$ruta_json,$fileName);
            if($sql){
                if(move_uploaded_file($tmp_name, './'.$ruta_excel)){
                    file_put_contents('./'.$ruta_json, $json);
                    $response = 1; 
                }else{
                    $response = 2;
                } 
            }
        }else{
            $response = 0;
        }
        

        /*$updatrue = '<div class="alert alert-success alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
                        <strong>Actualizados: </strong> '.$utrue.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $updafalse = '<div class="alert alert-warning alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                        <strong>No actualizados: </strong> '.$ufalse.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $insetrue = '<div class="alert alert-success alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
                        <strong>Nuevos registros: </strong> '.$itrue.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $insefalse = '<div class="alert alert-danger alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                        <strong>Errores: </strong> '.$ifalse.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $omitidos = '<div class="alert alert-info alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                        <strong>Omitidos: </strong> '.$omi.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';*/
               

        //$response = true;

}else{
    $response = 3;
}
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Catálogo Factura Emitidas</h4>
                    <span>Element</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Catálogos</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Catálogo Facturas Emitidas</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Listado por mes de Facturas Emitidas</h4>
                        <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal"
                            data-target="#articulonuevo">Agregar Fac. Emitidas</button>
                        <div class="modal fade" id="articulonuevo">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Agregar Facturas Emitidas</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="basic-form custom_file_input">
                                            <form id="form" name="frmload" method="post" action="./catfacemitidas"
                                                enctype="multipart/form-data">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <a id="btnfacs" class="btn btn-primary btn-sm text-white"
                                                            type="submit">Agregar</a>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" id="file" name="file"
                                                            class="custom-file-input">
                                                        <span id="file-label-edit" class="custom-file-label">Ningún
                                                            archivo seleccionado</span>
                                                        <button type="submit" id="submit" name="submit" class="fake-btn"
                                                            style="display:none;">Agregar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--<div class="modal-footer">
                                                    <button type="button" class="btn btn-sm btn-danger light" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-sm btn-primary">Agregar</button>
                                                </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                            echo '<input type="hidden" id="respuesta" value="'.$response.'">';
                        ?>
                        <div class="table-responsive">
                            <table id="example" class="display" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Archivo</th>
                                        <th>Mes</th>
                                        <th>Total</th>
                                        <th>Fecha Alta</th>
                                        <th>Estatus</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php echo select_cat_fac_emitidas(); ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                    <th class="text-center">#</th>
                                        <th>Archivo</th>
                                        <th>Mes</th>
                                        <th>Total</th>
                                        <th>Fecha Alta</th>
                                        <th>Estatus</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>