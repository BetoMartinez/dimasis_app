<?php
set_time_limit(4000);
include './conexion/funciones/select.php';
$show = false;
if(isset($_POST['show'])){

    $show = true;
}

if(isset($_POST['submit'])){
    
        $name = $_FILES['file']['name'];
        $tmp_name = $_FILES['file']['tmp_name'];
        $type = $_FILES['file']['type'];
                
        if($type == 'application/vnd.ms-excel'){
            // Extension excel 97
            $ext = 'xls';
        }else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            // Extension excel 2007 y 2010
            $ext = 'xlsx';
        }else{
            // Extension no valida
            $input = "Este archivo no es valido";
            echo "Este archivo no es valido";
            exit();
        }

	    $fileName = str_replace(".".$ext, "", $name);

	    require_once './vendor/PHPExcel/Classes/PHPExcel.php';
	    $archivo = $tmp_name;
	    $inputFileType = PHPExcel_IOFactory::identify($archivo);
	    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	    $objPHPExcel = $objReader->load($archivo);
	    $sheet = $objPHPExcel->getSheet(0); 
	    $highestRow = $sheet->getHighestRow(); 
	    $highestColumn = $sheet->getHighestColumn();

	    $i =1;
	    $utrue = 0;
	    $ufalse = 0;
        $itrue = 0;
        $ifalse = 0;
        $omi = 0;

	    for ($row = 1; $row <= $highestRow; $row++){
		    $cuenta = $sheet->getCell("A".$row)->getValue();
		    $descripcion = $sheet->getCell("B".$row, )->getValue();
            
            $revision = find_cuenta_cat_balanza($cuenta, $descripcion);

            switch ($revision) {
                case "Utrue":
                    $utrue++;
                break;
                case "Ufalse":
                  $ufalse++;
                break;
                case "Itrue":
                  $itrue++; 
                break;
                case "Ifalse":
                  $ifalse++;
                default:
                $omi++;
              }

		    $i++;
	    }

        $updatrue = '<div class="alert alert-success alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
                        <strong>Actualizados: </strong> '.$utrue.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $updafalse = '<div class="alert alert-warning alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                        <strong>No actualizados: </strong> '.$ufalse.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $insetrue = '<div class="alert alert-success alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
                        <strong>Nuevos registros: </strong> '.$itrue.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $insefalse = '<div class="alert alert-danger alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                        <strong>Errores: </strong> '.$ifalse.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';
        $omitidos = '<div class="alert alert-info alert-dismissible fade show">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                        <strong>Omitidos: </strong> '.$omi.'
                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
                    </div>';

        
                                
                                

        $response = true;

}else{
    $response = false;
}

?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Catálogo Balanza</h4>
                    <span>Element</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Catálogos</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Catálogo Balanza</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Listado de Cuentas</h4>
                        <div class="buttons">
                            <?php if($show == false){
                                        echo '<a id="show" class="btn btn-sm btn-primary mb-2 text-white" type="submit">Mostrar Catálogo</a>
                                        <form method="post" action="./catbalanza" style="display:none;">
                                        <button type="submit" id="show-table" name="show" value ="true" class="fake-btn">Show</button>
                                        </form>';
                                    }?>
                            <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal"
                                data-target="#articulonuevo">Actualizar Catálogo</button>
                        </div>
                        <div class="modal fade" id="articulonuevo">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Catálogo Balanza</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="basic-form custom_file_input">
                                            <form id="form" name="frmload" method="post" action="./catbalanza"
                                                enctype="multipart/form-data">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <a id="upload" class="btn btn-primary btn-sm text-white"
                                                            type="submit">Actualizar</a>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" id="file" name="file"
                                                            class="custom-file-input">
                                                        <span id="file-label-edit" class="custom-file-label">Ningún
                                                            archivo seleccionado</span>
                                                        <button type="submit" id="submit" name="submit" class="fake-btn"
                                                            style="display:none;">Actualizar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--<div class="modal-footer">
                                                    <button type="button" class="btn btn-sm btn-danger light" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-sm btn-primary">Agregar</button>
                                                </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                                if($response == true){
                                    echo $omitidos;
                                    echo $updatrue;
                                    echo $updafalse;
                                    echo $insetrue;
                                    echo $insefalse;
                                }
                            ?>
                        <div class="table-responsive">
                            <table id="example" class="display" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Descripción</th>
                                        <th>Cuenta</th>
                                        <th>Estatus</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($show == true){echo select_cat_balanza();} ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Descripción</th>
                                        <th>Cuenta</th>
                                        <th>Estatus</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>