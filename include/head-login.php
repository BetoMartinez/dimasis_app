<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>DIMASUR-APP</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.ico">
<link href="./css/style.css" rel="stylesheet">
<link href="./vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">