<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>DIMASUR-APP</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.ico">
<link href="./vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
<link rel="stylesheet" href="./vendor/chartist/css/chartist.min.css">
<link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet">
<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
<link href="./vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">