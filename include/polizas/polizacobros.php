<?php
set_time_limit(4000);
include './conexion/funciones/select.php';
if(isset($_POST['submit'])){
	$name = $_FILES['file']['name'];
    $tname = $_FILES['file']['tmp_name'];
    $type = $_FILES['file']['type'];
                
    if($type == 'application/vnd.ms-excel')
    {
        // Extension excel 97
        $ext = 'xls';
    }
    else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    {
        // Extension excel 2007 y 2010
        $ext = 'xlsx';
    }else{
        // Extension no valida
        $input = "Este archivo no es valido";
        echo "Este archivo no es valido";
        exit();
    }

	$fileName = str_replace(".".$ext, "", $name);
	$titleTable = "Vista Poliza ".$fileName;

	require_once './vendor/PHPExcel/Classes/PHPExcel.php';
	$archivo = "./files/".$name;
	$inputFileType = PHPExcel_IOFactory::identify($archivo);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($archivo);
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();

	$i =1;
	$data = array();
	$tabla = array();

	for ($row = 2; $row <= $highestRow; $row++){
		$folio = $sheet->getCell("A".$row)->getValue();
		$fecha_excel = $sheet->getCell("B".$row, )->getValue();
		$timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_excel);
		$fecha = date("Y-m-d H:i:s",$timestamp);
		$cliente = $sheet->getCell("C".$row)->getValue();
		$concepto = $sheet->getCell("D".$row)->getValue();
		$tc = $sheet->getCell("E".$row)->getValue();
		$totalmn = $sheet->getCell("F".$row)->getValue();
		$totalusd = $sheet->getCell("G".$row)->getValue();
		$pumn = floatval($sheet->getCell("H".$row)->getValue());
		$ivamn = floatval($sheet->getCell("I".$row)->getValue());
		//$tc = $sheet->getCell("J".$row)->getValue();
		$cbancaria = $sheet->getCell("K".$row)->getValue();
		$fdeposito_excel = $sheet->getCell("L".$row)->getValue();
		$timestampd = PHPExcel_Shared_Date::ExcelToPHP($fdeposito_excel);
		$fdeposito = date("Y-m-d H:i:s",$timestampd);
		$rubro = $sheet->getCell("M".$row)->getValue();
		$division = $sheet->getCell("N".$row)->getValue();
		$monprefijo = $sheet->getCell("O".$row)->getValue();
		$sucursal = $sheet->getCell("P".$row)->getValue();

	if($sucursal != "CONCENTRADORA"){
		$claveSuc = obtener_clave_sucursal($sucursal);
		$tipoFac = obtener_tipo_factura($folio, $claveSuc);

		if($tipoFac != false){
			$t = sizeof($data);
			if($t == 0){
				$segmentoSuc = obtener_segmento_sucursal($sucursal);
				$rubroFac = obtener_rubro_excel($rubro);
				$folioFactura = obtener_folio_factura($concepto, $claveSuc, $rubroFac);
					/*$rubroif = obtener_rubro($concepto, $claveSuc);
						if($rubroif == ""){
							$rubroFac = obtener_rubro_excel($rubro);
						}else{
							$rubroFac = $rubroif;
						}*/
				array_push($data, 
							['folio' => $folio,
							'cuentab' => $cbancaria,
							'total' => $totalmn,
							'totalus' => $totalusd,
							'base' => $pumn,
							'pumn' => $pumn,
							'iva' => $ivamn,
							'cliente' => $cliente,
							'concepto' => $concepto,
							'clavesuc' => $claveSuc,
							'segmento' => $segmentoSuc,
							'rubro' => $rubro,
							'tipofac' => $tipoFac,
							'poliza' => array(
								['folio' => $folio,
									'foliofactura' => $folioFactura, 
									'fecha' => $fecha, 
									'cliente' => $cliente, 
									'concepto' => $concepto, 
									'tc' => $tc,
									'totalmn' => $totalmn, 
									'totalusd' => $totalusd, 
									'pumn' => $pumn, 
									'ivamn' => $ivamn, 
									'cbancaria' => $cbancaria,
									'fdeposito' => $fdeposito, 
									'rubro' => $rubroFac,
									'rubroex' => $rubro, 
									'division' => $division, 
									'monprefijo' => $monprefijo, 
									'sucursal' => $sucursal
								])
							]);
			}else{
				$fol = array_column($data, 'folio');
					/*$rubroif = obtener_rubro($concepto, $claveSuc);
						if($rubroif == ""){
							$rubroFac = obtener_rubro_excel($rubro);
						}else{
							$rubroFac = $rubroif;
						}*/

				if(in_array($folio, $fol)){
					$rubroFac = obtener_rubro_excel($rubro);
					$folioFactura = obtener_folio_factura($concepto, $claveSuc, $rubroFac);

					$key = array_search($folio, $fol);
					$sub = $data[$key]["total"];
					$total = $sub + $totalmn;
					$data[$key]["total"] = $total;

					$subb = $data[$key]["base"];
					$totalb = $subb + $pumn;
					$data[$key]["base"] = $totalb;

					$subi = $data[$key]["iva"];
					$totali = $subi + $ivamn;
					$data[$key]["iva"] = $totali;

					$subus = $data[$key]["totalus"];
					$totalus = $subus + $totalusd;
					$data[$key]["totalusd"] = $totalus;

					$array_tmp = array('folio' => $folio,
					'foliofactura' => $folioFactura,  
					'fecha' => $fecha, 
					'cliente' => $cliente, 
					'concepto' => $concepto, 
					'tc' => $tc,
					'totalmn' => $totalmn, 
					'totalusd' => $totalusd, 
					'pumn' => $pumn, 
					'ivamn' => $ivamn, 
					'cbancaria' => $cbancaria,
					'fdeposito' => $fdeposito, 
					'rubro' => $rubroFac,
					'rubroex' => $rubro, 
					'division' => $division, 
					'monprefijo' => $monprefijo, 
					'sucursal' => $sucursal);

					array_push($data[$key]["poliza"], $array_tmp);
					$array_tmp = array();
					
				}else{
					$segmentoSuc = obtener_segmento_sucursal($sucursal);
					$rubroFac = obtener_rubro_excel($rubro);
					$folioFactura = obtener_folio_factura($concepto, $claveSuc, $rubroFac);
					/*$rubroif = obtener_rubro($concepto, $claveSuc);
						if($rubroif == ""){
							$rubroFac = obtener_rubro_excel($rubro);
						}else{
							$rubroFac = $rubroif;
						}*/
					array_push($data, 
							['folio' => $folio, 
							'cuentab' => $cbancaria,
							'total' => $totalmn,
							'totalus' => $totalusd,
							'base' => $pumn,
							'pumn' => $pumn,
							'iva' => $ivamn,
							'cliente' => $cliente,
							'concepto' => $concepto,
							'clavesuc' => $claveSuc,
							'segmento' => $segmentoSuc,
							'rubro' => $rubro,
							'tipofac' => $tipoFac,  
							'poliza' => array(
								['folio' => $folio,
									'foliofactura' => $folioFactura, 
									'fecha' => $fecha, 
									'cliente' => $cliente, 
									'concepto' => $concepto, 
									'tc' => $tc,
									'totalmn' => $totalmn, 
									'totalusd' => $totalusd, 
									'pumn' => $pumn, 
									'ivamn' => $ivamn, 
									'cbancaria' => $cbancaria,
									'fdeposito' => $fdeposito,
									'rubroex' => $rubro, 
									'rubro' => $rubroFac, 
									'division' => $division, 
									'monprefijo' => $monprefijo, 
									'sucursal' => $sucursal
								]) 
							]);
				}
				
			}
		}
	}
		$i++;
	}

	$polizaf = array();
	$rubrosfac = array('a' => array(),
						'b' => array(),
						'c' => array(),
					);
	$abonosarr = array();
	foreach ($data as $key => $value) {

		$cuentaBanco = cuenta_banco($value['cuentab']);
		$nombreCuentaBanco = nombre_cuenta_banco($value['cuentab']);
		$cuentaCargo = cuenta_cargo($value['base'], $value['iva']);
		$cuentaAbono = cuenta_abono($value['base'], $value['iva']);
		$cuentaIvaCargo = cuenta_iva_cargo($value['base'], $value['iva']);
		$cuentaIvaAbono = cuenta_iva_abono($value['base'], $value['iva'], $value['folio']);
		$diario = Obtener_diario($value['rubro'], $value['iva']);


		$polizaarr = array('cuentabanco' => $cuentaBanco, 
							'nombre' => $nombreCuentaBanco, 
							'cargome' => '', 
							'cargo' => $value['total'], 
							'rubro' => $value['rubro'],
							'tipofac' => $value['tipofac'],
							'referencia' => $value['folio'],
							'concepto' => $value['cliente'],
							'cuentacargo' => $cuentaCargo,
							'cuentaabono' => $cuentaAbono,
							'cuentaivacargo' => $cuentaIvaCargo,
							'cuentaivaabono' => $cuentaIvaAbono,
							'diario' => $diario,
							'nosegmento' => $value['segmento'],
							'abonos' => array(),
							'base' => $value['base'],
							'iva' => $value['iva']
							);

		foreach ($value['poliza'] as $i => $val) {

			$folioFactura = $val['foliofactura'];
			//$cuentaCliente = cuenta_cliente($value['rubro']);
			$cuentaClienteFac = cuenta_cliente($value['cliente'],$val['rubro'],$value['iva'], $val['concepto'], $value['tipofac']);
			//var_dump($cuentaClienteFac."<br/>");
			//$diarioFac = Obtener_diario($val['rubro'],$value['iva']);
			if($val['ivamn'] > 0){
				$ivaFac = ($val['ivamn']*100)/$val['pumn'];
			}else{
				$ivaFac = 0;
			}

			/*$ta = sizeof($rubrosfac);
			if($ta == 0){
				$rubrosfac = array('a' => $folioFactura,
								'b' => $ivaFac,
								'c' => $val['totalmn'],
							);
			}else{
				$poliza = $value['poliza'][$i];
				$totalmnFac = $value['poliza'][$i]['totalmn'];
				$pumnFac = $value['poliza'][$i]['pumn'];
				$ivaFac = $value['poliza'][$i]['ivamn'];

				$fac = array_column($poliza, 'foliofactura');
				if(in_array($folioFactura, $fac)){
					$key = array_search($folioFactura, $fac);
					$sub = $poliza[$key]["totalmn"];
					$total = $sub + $totalmn;
					$poliza[$key]["total"] = $total;

					$subb = $poliza[$key]["base"];
					$totalb = $subb + $pumn;
					$poliza[$key]["base"] = $totalb;

					$subi = $poliza[$key]["iva"];
					$totali = $subi + $ivamn;
					$poliza[$key]["iva"] = $totali;
				}
			}*/

			

			$abonosarr = array('folioFactura' => $folioFactura,
								'folio' => $value['folio'],
								'iva' => $ivaFac,
								'abonot' => $val['totalmn'],
								'abonop' => $val['pumn'],
								'abonous' => $val['totalusd'],
								'cuentacliente' => $cuentaClienteFac,
							);
			
			array_push($polizaarr['abonos'], $abonosarr);
		}
			
		array_push($polizaf, $polizaarr);

	}
    	$print = table($polizaf);

}else{
	$titleTable = "Generadora Póliza de Cobros";
    $print = "Generador de póliza cobros base XLS de ingresos; ¡Sube tu archivo y genera su vista previa para descargar la póliza!";
}
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Póliza de Cobros</h4>
                    <span>Element</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Polizas</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Póliza Cobros</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Seleccionar archivo</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form custom_file_input">
                                    <form id="form" name="frmload" method="post" action="./polizacobros" enctype="multipart/form-data">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <a id="upload" class="btn btn-primary btn-sm text-white" type="submit">Vista previa</a>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" id="file" name="file" class="custom-file-input">
                                                <span id="file-label-edit" class="custom-file-label">Ningún archivo seleccionado</span>
                                                <button type="submit" id="submit" name="submit" class="fake-btn" style="display:none;">Vista Previa</button>
                                            </div>
                                        </div>
                                    </form>
            </div>
            <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title text-uppercase"><?php echo $titleTable;?></h4>
								<?php
									if(isset($_POST['submit'])){
										echo '<a id="click" name="'.$fileName.'" class="btn btn-primary btn-sm text-white" style="cursor:pointer;">Descargar Póliza</a>';
									}
								?>
                            </div>
                            <div class="card-body">
                                <?php echo $print;?>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
