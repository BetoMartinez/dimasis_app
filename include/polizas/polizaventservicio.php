<?php
set_time_limit(40000);
include './conexion/funciones/select.php';
if(isset($_POST['submit'])){
	$name = $_FILES['file']['name'];
    $tname = $_FILES['file']['tmp_name'];
    $type = $_FILES['file']['type'];
                
    if($type == 'application/vnd.ms-excel')
    {
        // Extension excel 97
        $ext = 'xls';
    }
    else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    {
        // Extension excel 2007 y 2010
        $ext = 'xlsx';
    }else{
        // Extension no valida
        $input = "Este archivo no es valido";
        echo "Este archivo no es valido";
        exit();
    }

	$fileName = str_replace(".".$ext, "", $name);
	$titleTable = "Vista Poliza ".$fileName;

	require_once './vendor/PHPExcel/Classes/PHPExcel.php';
	$archivo = "./files/".$name;
	$inputFileType = PHPExcel_IOFactory::identify($archivo);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($archivo);
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();

	$i =1;
	$data = array();
	$tabla = array();

	for ($row = 2; $row <= $highestRow; $row++){
		$sucursal = $sheet->getCell("A".$row)->getValue();
		$folio = $sheet->getCell("B".$row, )->getValue();
        $fecha_excel = $sheet->getCell("C".$row, )->getValue();
        $fecha_trans = str_replace('/', '-', $fecha_excel);
		$timestamp = new DateTime($fecha_trans);
		$fecha = $timestamp->format('d-m-Y');
		$cliente = $sheet->getCell("D".$row)->getValue();
		$ot = $sheet->getCell("E".$row)->getValue();
		$moneda = $sheet->getCell("F".$row)->getValue();
		$tdc = $sheet->getCell("G".$row)->getValue();
		$serie = $sheet->getCell("H".$row)->getValue();
		$manoObra = floatval($sheet->getCell("I".$row)->getValue());
		$refacciones = floatval($sheet->getCell("J".$row)->getValue());
		$descuento = $sheet->getCell("K".$row)->getValue();
		$tforaneo = $sheet->getCell("L".$row)->getValue();
		$kilometraje = $sheet->getCell("M".$row)->getValue();
		$varios = $sheet->getCell("N".$row)->getValue();
		$subtotal = $sheet->getCell("O".$row)->getValue();
		$impuestos = $sheet->getCell("P".$row)->getValue();
		//$retencionPorcentaje = $sheet->getCell("Q".$row)->getValue();
        //$retencionImporte = $sheet->getCell("R".$row)->getValue();
        $totalFactura = $sheet->getCell("S".$row)->getValue();
        $tipoCargo = $sheet->getCell("T".$row)->getValue();
        $division = $sheet->getCell("U".$row)->getValue();
		$cond = $sheet->getCell("V".$row)->getValue();
		$mecanicos = $sheet->getCell("W".$row)->getValue();
        $estatus = $sheet->getCell("X".$row)->getValue();
        $saldo = $sheet->getCell("Y".$row)->getValue();
        $ultimoPago_excel = $sheet->getCell("Z".$row, )->getValue();
        if($ultimoPago_excel != ' '){
            //$ultimoPago_excel_trans = str_replace('/', '-', $ultimoPago_excel);
		    $timestam = PHPExcel_Shared_Date::ExcelToPHP($ultimoPago_excel);
		    $ultimoPago = date("Y-m-d H:i:s",$timestam);
        }else{
            $ultimoPago = 0;
        }

        array_push($data, 
							['sucursal' => $sucursal,
                            'folio' => $folio,
							'fecha' => $fecha,
							'cliente' => $cliente,
							'ot' => $ot,
							'moneda' => $moneda,
							'tdc' => $tdc,
							'serie' => $serie,
							'manoobra' => $manoObra,
							'refacciones' => $refacciones,
							'descuento' => $descuento,
							'tforaneo' => $tforaneo,
							'kilometraje' => $kilometraje,
							'varios' => $varios,
                            'subtotal' => $subtotal,
                            'impuestos' => $impuestos,
                            //'retencionporcentaje' => $retencionPorcentaje,
                            //'retencionimporte' => $retencionImporte,
                            'totalfactura' => $totalFactura,
                            'tipocargo' => $tipoCargo,
                            'division' => $division,
                            'cond' => $cond,
                            'mecanicos' => $mecanicos,
                            'estatus' => $estatus,
                            'saldo' => $saldo,
                            'ultimopago' => $ultimoPago,
							]);

		$i++;
	}

    	$print = table_poliza_ventas_servicio($data);

}else{
	$titleTable = "Generadora Póliza de Ventas Servicio";
    $print = "Generador de póliza de ventas de servicio base XLS Servicio DOF; ¡Sube tu archivo y genera su vista previa para descargar la póliza!";
}
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Póliza Ventas Servicio</h4>
                    <span>Element</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Polizas</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Póliza Ventas Servicio</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Seleccionar archivo</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form custom_file_input">
                                    <form id="form" name="frmload" method="post" action="./polizaventservicio" enctype="multipart/form-data">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <a id="upload" class="btn btn-primary btn-sm text-white" type="submit">Vista previa</a>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" id="file" name="file" class="custom-file-input">
                                                <span id="file-label-edit" class="custom-file-label">Ningún archivo seleccionado</span>
                                                <button type="submit" id="submit" name="submit" class="fake-btn" style="display:none;">Vista Previa</button>
                                            </div>
                                        </div>
                                    </form>
            </div>
            <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title text-uppercase"><?php echo $titleTable;?></h4>
								<?php
									if(isset($_POST['submit'])){
										echo '<a id="click" name="'.$fileName.'" class="btn btn-primary btn-sm text-white" style="cursor:pointer;">Descargar Póliza</a>';
									}
								?>
                            </div>
                            <div class="card-body">
                                <?php echo $print;?>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
