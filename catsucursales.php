<?php 
session_start();
if(!isset($_SESSION['userid'])){
    header('Location: ./login');
}else{
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    $shortname = $_SESSION['shortname'];
	$shortlastname = $_SESSION['shortlastname'];
    $permisos = $_SESSION['tipous'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './include/head.php'?>
    <!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <?php include './include/loader.php'?>
    <div id="main-wrapper">
        <?php include './include/header.php'?>
        <?php include './include/nav.php'?>
        <?php include './include/catalogos/catsucursales.php'?>
    </div>
    <?php include './include/scripts.php'?>
    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
    <script>
    $(document).ready(function() {
        
    });

    $("#btnfrmsuc").click(function() {
        if($("#suc").val() == ""){
            Swal.fire({
                    icon: 'warning',
                    title: 'Oops!',
                    text: 'Campos requeridos',
                    confirmButtonText: 'Ok'
                });
        }else{
            var postData = $('#frmsuc').serializeArray();
            var formURL = "./conexion/funciones/catalogos/registrar_sucursal.php";
        $.ajax({
            url : formURL,
                type: "POST",
                async: false,
                data : postData,
            //mientras enviamos el archivo
            beforeSend: function() {
                Swal.fire({
                    icon: 'info',
                    title: 'Trabajando!',
                    text: 'Espere un momento...',
                    showConfirmButton: false,
                });
            },
            success: function(data) {
                if(data == 1){
                    let timerInterval
                    Swal.fire({
                        icon: 'success',
                        title: 'Exito!',
                        html: 'Sucursal agregada, la página se actualizará en 5 segundos.',
                        timer: 5000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                            location.reload()
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    })
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Contacte a Sistemas',
                    text: data,
                    showConfirmButton: true,
                });
                }
                    
            },
            error: function(jqXHR, textStatus, datos) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: datos,
                    confirmButtonText: 'Ok'
                });
            }
        });
        }
    });
    </script>
</body>
</html>