<?php 
session_start();
if(!isset($_SESSION['userid'])){
    header('Location: ./login');
}else{
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    $shortname = $_SESSION['shortname'];
	$shortlastname = $_SESSION['shortlastname'];
    $permisos = $_SESSION['tipous'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './include/head.php'?>
    <!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <?php include './include/loader.php'?>
    <div id="main-wrapper">
        <?php include './include/header.php'?>
        <?php include './include/nav.php'?>
        <?php include './include/catalogos/catusuarios.php'?>
    </div>
    <?php include './include/scripts.php'?>
    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
    <script>
    $(document).ready(function() {
        
    });

    $("#btnfrmuser").click(function() {
        if($("#nombre").val() == ""){
            Swal.fire({
                    icon: 'warning',
                    title: 'Oops!',
                    text: 'Campos requeridos',
                    confirmButtonText: 'Ok'
                });
        }else{
            var postData = $('#frmuser').serializeArray();
            var formURL = "./conexion/funciones/usuarios/registrar.php";
        $.ajax({
            url : formURL,
                type: "POST",
                async: false,
                data : postData,
            //mientras enviamos el archivo
            beforeSend: function() {
                Swal.fire({
                    icon: 'info',
                    title: 'Trabajando!',
                    text: 'Espere un momento...',
                    showConfirmButton: false,
                });
            },
            success: function(data) {
                if(data == 1){
                    let timerInterval
                    Swal.fire({
                        icon: 'success',
                        title: 'Exito!',
                        html: 'Usuario agregado, la página se actualizará en 5 segundos.',
                        timer: 5000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                            location.reload()
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    })
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Contacte a Sistemas',
                    text: data,
                    showConfirmButton: true,
                });
                }
                    
            },
            error: function(jqXHR, textStatus, datos) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: datos,
                    confirmButtonText: 'Ok'
                });
            }
        });
        }
    });


    $("#btncontrasenia").click(function() {
        if($("#contrasenaid").val() == ""){
            Swal.fire({
                    icon: 'warning',
                    title: 'Oops!',
                    text: 'Campos requeridos',
                    confirmButtonText: 'Ok'
                });
        }else{
            var postData = $('#frmusercontrasenia').serializeArray();
            var formURL = "./conexion/funciones/usuarios/contrasenia.php";
        $.ajax({
            url : formURL,
                type: "POST",
                async: false,
                data : postData,
            //mientras enviamos el archivo
            beforeSend: function() {
                Swal.fire({
                    icon: 'info',
                    title: 'Trabajando!',
                    text: 'Espere un momento...',
                    showConfirmButton: false,
                });
            },
            success: function(data) {
                if(data == 1){
                    let timerInterval
                    Swal.fire({
                        icon: 'success',
                        title: 'Exito!',
                        html: 'Contraseña actualizada, la página se actualizará en 5 segundos.',
                        timer: 5000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                            location.reload()
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    })
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Contacte a Sistemas',
                    text: data,
                    showConfirmButton: true,
                });
                }
                    
            },
            error: function(jqXHR, textStatus, datos) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: datos,
                    confirmButtonText: 'Ok'
                });
            }
        });
        }
    });

    $('#btnaddus').click(function(){
        $('#modo').val(0);
        $('#titleus').text("Agregar Nuevo");
        $('#btnfrmuser').text("Agregar");
        $('#hiddencon').show();
        $('#nombre').val("");
        $('#apellido').val("");
        $('#correo').val("");
        $('#tipous').val("");
    });

    function cargardatos(id){
            var formURL = "./conexion/funciones/catalogos/getDataUsuarios.php";
            $.ajax({
                url : formURL,
                type: "POST",
                datetype: "json",
                async: false,
                data : {'id' : id},
                success:function(data,textStatus) 
                {
                  //alert(data);
                 data = JSON.parse(data);
                    $('#modo').val(1);
                    $('#id').val(id);
                    $('#nombre').val(data.nombre);
                    $('#apellido').val(data.apellido);
                    $('#correo').val(data.correo);
                    $('#tipous').val(data.id_tipoUsuario);
                    $('#hiddencon').hide();
                    $('#titleus').text("Editar");
                    $('#btnfrmuser').text("Editar");
                    $("#modalUsuario").modal("show");
                },
                error: function(jqXHR, textStatus) 
                {
                //alert(textStatus);
                    $("#errormsg").html(textStatus);
                }
            });
          }

          function cargarid(id){
            $('#idcontrasenia').val(id);
            $('#contrasenaid').val("");
          }

    </script>
</body>
</html>