<?php
require __DIR__.'/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
$contenido = $_POST['contenido'];
$desc = $_POST['desc'];
$insumos = $_POST['insumos'];
$marca = $_POST['marca'];
$hecho = $_POST['hecho'];
$importado = $_POST['importado'];
$dire = $_POST['dire'];
$tel = $_POST['tel'];
$rfc = $_POST['rfc'];;
$cantidad = $_POST['cantidad'];

require_once 'print_view.php';
$html = ob_get_clean();

$html2pdf = new Html2Pdf('p', 'letter', 'es', 'true', 'UTF-8');
$html2pdf -> writeHTML($html);
$html2pdf ->output();
?>