<?php 
session_start();
if(!isset($_SESSION['userid'])){
    header('Location: ./login');
}else{
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    $shortname = $_SESSION['shortname'];
	$shortlastname = $_SESSION['shortlastname'];
    $permisos = $_SESSION['tipous'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './include/head.php'?>
</head>
<body>
    <?php include './include/loader.php'?>
    <div id="main-wrapper">
        <?php include './include/header.php'?>
        <?php include './include/nav.php'?>
        <?php include './include/polizas/polizacobros.php'?>
    </div>
    <?php include './include/scripts.php'?>
    <script>
    $(document).ready(function() {
        var fileInput = $('.custom-file-input');

        fileInput.on('change', function() {
            var filesCount = $(this)[0].files.length;
            var textContainer = $(".custom-file-label");


            if (filesCount === 1) {
                // if single file then show file name
                textContainer.html($(this).val().split('\\').pop());
            }else {
                // otherwise show number of files
                textContainer.text(filesCount + ' files selected');
            }
        });
    });

    $("#upload").click(function() {
        if($("#file").val() == ""){
            Swal.fire({
                    icon: 'warning',
                    title: 'Oops!',
                    text: 'Debes seleccionar un archivo',
                    confirmButtonText: 'Ok'
                });
        }else{
            var postData = new FormData($("#form")[0]);
        var formURL = "./include/polizas/upload";
        $.ajax({
            url: formURL,
            type: "POST",
            data: postData,
            //cache: false,
            contentType: false,
            processData: false,
            async: false,
            //mientras enviamos el archivo
            beforeSend: function() {
                Swal.fire({
                    icon: 'info',
                    title: 'Trabajando!',
                    text: 'Espere un momento...',
                    showConfirmButton: false,
                });
            },
            success: function(data) {
                if(data == 1){
                    $("#submit").click()
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Contacte a Sistemas',
                    text: data,
                    showConfirmButton: false,
                });
                }
                    
            },
            error: function(jqXHR, textStatus, datos) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: datos,
                    confirmButtonText: 'Ok'
                });
                //alert('Error ajax');
            }
        });
        }
    });

    $('#click').click(function(){
        var file = this.name.toUpperCase()
        $("#poliza").table2excel({
            exclude:".noExl",
            name:"Worksheet Name",
            filename:"PÓLIZA "+file,//do not include extension
            fileext:".xlsx" // file exten
          });
    });
</script>
</body>
</html>